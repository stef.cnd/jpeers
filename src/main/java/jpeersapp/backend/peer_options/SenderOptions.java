package jpeersapp.backend.peer_options;


/**
 * Class that models the set of options belonging to
 * a sender peer;
 */
public class SenderOptions extends DefaultPeerOptions {

    private int sendFrequency;
    private int incrementalCounter;
    private String sendMode;


    /**
     * Constructor
     */
    public SenderOptions() {
        super();

        this.sendMode = "";
        this.incrementalCounter = DEFAULT_INCREMENTAL_COUNTER;
        this.sendFrequency = DEFAULT_SEND_FREQUENCY;
    }


    /**
     *
     */
    public void setSendMode(String selectedMode) {
        this.sendMode = selectedMode;
    }


    /**
     * Set the starting value of the incremental counter;
     */
    public void setIncrementalCounter(int counter) {
        this.incrementalCounter = counter;
    }


    /**
     * Set the sender frequency;
     */
    public void setSendFrequency(String newFrequency) {
        if (!newFrequency.isEmpty()) {
            this.sendFrequency = Integer.parseInt(newFrequency);
        }
    }


    /**
     * Get the sending mode;
     */
    public String getSendMode() {
        return this.sendMode;
    }


    /**
     * Get the sender frequency;
     */
    public int getSendFrequency() {
        return this.sendFrequency;
    }


    /**
     * Get the starting value of the incremental counter;
     */
    public int getIncrementalCounter() {
        return this.incrementalCounter;
    }
}