package jpeersapp.backend.udp_peers;


import java.net.*;
import java.io.IOException;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.Logger;
import java.nio.charset.StandardCharsets;
import org.apache.logging.log4j.LogManager;
import jpeersapp.backend.peer_options.SenderOptions;


/**
 * Class that models a sender UDP peer;
 */
public class UdpSenderPeer extends UdpPeer {

    private byte[] sendBuffer;
    public SenderOptions options;
    private static final Logger udpSenderLogger = LogManager.getLogger(UdpSenderPeer.class.getName());


    /**
     * Constructor;
     * sendBuffer and executor are initialised respectively
     * in setSendBufferData and startSending methods;
     */
    public UdpSenderPeer() {
        super();

        this.options = new SenderOptions();
    }


    /**
     * Initialise a new datagram sender socket;
     */
    @Override
    public void initialiseSocket() {
        try {
            this.socket = new DatagramSocket();
        } catch (SocketException e) {
            System.out.println(e.getMessage());
        }
    }


    /**
     * Initialise a new datagram packet with all the current
     * socket attributes;
     */
    @Override
    public void initialisePacket() {
        this.packet = new DatagramPacket(sendBuffer, sendBuffer.length, this.address, this.port);
    }


    /**
     * Set the sender buffer data of the UDP peer (raw content);
     */
    public void setSendBufferData(byte[] buffer) {
        this.sendBuffer = buffer;
    }


    /**
     * Set a new size of the send buffer;
     * Note: this is used only in case of sending mode "Arbitrary Size";
     */
    public void setArbitrarySize(int newSize) {
        this.sendBuffer = new byte[newSize];
    }


    /**
     * Increase the counter value;
     * Note this is used only in case of sending mode "Incremental Counter";
     */
    public void increaseCounter(int counter) {
        this.options.setIncrementalCounter(counter+1);
        String counterString = Integer.toString(counter);
        this.setSendBufferData(counterString.getBytes(StandardCharsets.UTF_8));
    }


    /**
     * Set the sending related actions;
     * Create a new thread for the current peer;
     */
    public void startSending() {
        this.executor = Executors.newCachedThreadPool();

        Runnable task = () -> {
            while (!this.executor.isShutdown()) {
                try {
                    switch (this.options.getSendMode()) {
                        case "Custom Payload" -> { /* placeholder */ }
                        case "Incremental Counter" -> {
                            this.increaseCounter(this.options.getIncrementalCounter());
                            this.packet.setData(this.sendBuffer);
                        }
                        case "Arbitrary Size" -> { /* placeholder */ }
                        default -> { System.out.println("WARNING: Sending mode not valid"); }
                    }

                    this.socket.connect(this.address, this.port);
                    this.socket.send(this.packet);
                    udpSenderLogger.info(buildLogMessage());
                    this.socket.disconnect();

                    Thread.sleep(this.options.getSendFrequency());
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        if (this.socket != null) {
            this.executor.execute(task);
        }
    }


    /**
     * Assembles the login message associated with a single
     * UDP send action. The format is:
     * yyyy-MM-dd HH:mm:ss.SSS - senderIP:senderPort  --> receiverIp:receiverPort  Len=packetLen
     */
    protected String buildLogMessage() {
        return String.format("%1$-25s %2$-25s %3$-20s %4$s",
                "Src: " + this.socket.getLocalAddress().getHostAddress(),
                "Dst: " + this.packet.getAddress().getHostAddress(),
                this.socket.getLocalPort() + " --> " + this.port,
                "Len=" + this.packet.getLength());
    }
}