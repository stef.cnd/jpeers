package jpeersapp.frontend.udp_peers;


import jpeersapp.backend.generic_peer.AbstractPeer;
import jpeersapp.backend.udp_peers.UdpReceiverPeer;
import jpeersapp.frontend.generic_peers.GuiPeerState;
import jpeersapp.frontend.generic_peers.GuiReceiverPeer;
import jpeersapp.frontend.tabs.peer_choice.PeerChoice;


/**
 * This class models the GUI equivalent of a UDP receiver peer;
 */
public class GuiUdpReceiverPeer extends GuiReceiverPeer {

    private final UdpReceiverPeer udpReceiverPeer;


    /**
     * Constructor
     */
    public GuiUdpReceiverPeer(Integer peerId, PeerChoice choice) {
        super(peerId, choice);

        this.udpReceiverPeer = new UdpReceiverPeer();

        this.setRecvButton();
        this.setStopButton();
    }


    /**
     * Collect user input on UDP receiver properties;
     * Set the UDP receiver properties (backend);
     */
    protected void collectAndSetPeerProperties() {
        this.udpReceiverPeer.setPeerAddress(this.addressField.getText());
        this.udpReceiverPeer.setPeerPort(Integer.parseInt(this.portField.getText()));
    }


    /**
     * Set the Recv button actions and color behavior;
     */
    private void setRecvButton() {
        this.recvButton.setOnAction(e -> {

            if (!this.state.equals(GuiPeerState.RECEIVING)) {
                this.recvButton.setStyle("-fx-text-fill: green");
                this.stopButton.setStyle("-fx-text-fill: black");

                this.collectAndSetPeerProperties();
                this.udpReceiverPeer.initialisePacket();
                this.udpReceiverPeer.initialiseSocket();
                this.udpReceiverPeer.startReceiving();

                this.state = GuiPeerState.RECEIVING;
            }
        });
    }


    /**
     * Set the Stop button actions and color behavior;
     */
    private void setStopButton() {
            this.stopButton.setOnAction(e -> {

            if (!this.state.equals(GuiPeerState.IDLE)) {
                this.stopButton.setStyle("-fx-text-fill: red");
                this.recvButton.setStyle("-fx-text-fill: black");

                this.udpReceiverPeer.closePeer(this.udpReceiverPeer.getSocket());
                this.state = GuiPeerState.IDLE;
            }
        });
    }


    /**
     * Apply new options to the UDP receiver peer (backend);
     */
    @Override
    public void applyNewOptions() {
        this.udpReceiverPeer.options.setRecvFrequency(
                this.optionsWindow.receiverOptions.gui_getRecvFrequency());

        this.udpReceiverPeer.options.setReceiverBufferSize(
                this.optionsWindow.receiverOptions.gui_getReceiverBufferSize());
    }


    /**
     * Get the actual UDP receiver backend peer encapsulated in the guiPeer;
     */
    @Override
    public AbstractPeer getPeer() {
        return this.udpReceiverPeer;
    }
}