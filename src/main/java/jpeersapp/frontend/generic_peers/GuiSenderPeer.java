package jpeersapp.frontend.generic_peers;


import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import jpeersapp.frontend.tabs.peer_choice.PeerChoice;


/**
 * Abstract class that models the GUI equivalent of a generic
 * sender peer;
 */
public abstract class GuiSenderPeer extends GuiPeer {

    private final Label senderPayloadLabel;
    protected TextField senderPayloadField;
    protected String selectedSendingMode;


    /**
     * Constructor
     */
    protected GuiSenderPeer(Integer peerId, PeerChoice choice) {
        super(peerId, choice);

        this.senderPayloadLabel = new Label("Payload");
        this.senderPayloadField = new TextField();
        this.sendButton = new Button("Send");
        this.stopButton = new Button("Stop");

        this.setGuiPeerLook();
    }


    /**
     * Set the look of the sender container and of the items in it;
     */
    @Override
    protected void setGuiPeerLook() {
        super.setGuiPeerLook();

        this.sendButton.setMinWidth(60.0);
        this.stopButton.setMinWidth(60.0);
        this.sendButton.setAlignment(Pos.BASELINE_CENTER);
        this.stopButton.setAlignment(Pos.BASELINE_CENTER);
    }


    /**
     * Add items to the sender peer container;
     */
    @Override
    public void addItemsToGuiPeer() {
        super.addItemsToGuiPeer();

        this.guiPeerContainer.getChildren().addAll(this.senderPayloadLabel,
                this.senderPayloadField,
                this.sendButton,
                this.stopButton,
                this.optionsWindow.openOptionsButton);
    }


    /**
     * Get the currently selected sending mode;
     */
    protected void getSendingMode() {
        RadioButton selectedRB = (RadioButton) this.optionsWindow.senderOptions.gui_getSelectedRB();
        this.selectedSendingMode = selectedRB.getText();
    }
}