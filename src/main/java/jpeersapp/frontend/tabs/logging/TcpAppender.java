package jpeersapp.frontend.tabs.logging;


import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;
import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


/**
 * Log4j2 Appender: Tcp appender
 */
@Plugin(
        name = "TcpAppender",
        category = "Core",
        elementType = "appender",
        printObject = true)
public final class TcpAppender extends AbstractAppender {

    private static ObservableList<String> logObservableList;
    private static ListView<String> logListView;
    private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final Lock readLock = rwLock.readLock();
    private final int MAX_LOG_ENTRIES;


    /**
     * Constructor
     */
    public TcpAppender(String name,
                          Filter filter,
                          Layout<? extends Serializable> layout,
                          final boolean ignoreExceptions,
                          Property[] properties) {
        super(name, filter, layout, ignoreExceptions, properties);
        this.MAX_LOG_ENTRIES = 10000;
    }


    /**
     * This method is where the appender does the work.
     *
     * @param event Log event with log data
     */
    @Override
    public void append(LogEvent event) {
        readLock.lock();

        final String message = new String(getLayout().toByteArray(event));

        /* append log text to ObservableList */
        try {
            Platform.runLater(() -> {
                try {
                    if (logObservableList.size() >= MAX_LOG_ENTRIES) {
                        logObservableList.clear();
                    }
                    logObservableList.add(message);
                    logListView.scrollTo(logObservableList.size() - 1);
                } catch (final Throwable t) {
                    System.out.println(t.getMessage());
                }
            });
        } catch (final IllegalStateException ex) {
            ex.printStackTrace();

        } finally {
            readLock.unlock();
        }
    }


    /**
     * Factory method. Log4j will parse the configuration and call this factory
     * method to construct the appender with the configured attributes.
     *
     * @param name   Name of appender
     * @param layout Log layout of appender
     * @param filter Filter for appender
     * @return The TcpAppender
     */
    @PluginFactory
    public static TcpAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginElement("Properties") Property[] properties) {
        if (name == null) {
            LOGGER.error("No name provided for TcpAppender");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new TcpAppender(name, filter, layout, true, properties);
    }


    /**
     * Set TextArea to append
     *
     * @param logList logArea to append
     */
    public static void setLogArea(ObservableList<String> logObsList, ListView<String> logList) {
        TcpAppender.logObservableList = logObsList;
        logListView = logList;
    }
}