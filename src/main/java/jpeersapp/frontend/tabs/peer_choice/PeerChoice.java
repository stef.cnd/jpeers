package jpeersapp.frontend.tabs.peer_choice;

/**
 * Enum that models the possible peer choices;
 */
public enum PeerChoice {
    SENDER,
    RECEIVER,
    PAIR;
}
