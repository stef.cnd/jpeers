package jpeersapp.frontend.tabs.peer_choice;

import javafx.scene.control.ChoiceBox;


/**
 * Class that models the possible peer choices a user
 * can make. Choices are made interacting with a ChoiceBox
 * encapsulated in this class;
 */
public class PeerChoiceBox {

    private final ChoiceBox peerTypeChoice;


    /**
     * Constructor
     */
    public PeerChoiceBox() {
        this.peerTypeChoice = new ChoiceBox();

        this.setPossibleChoices();
    }


    /**
     * Set possible choices in the ChoiceBox list;
     */
    private void setPossibleChoices() {
        this.peerTypeChoice.getItems().setAll(PeerChoice.values());

        /* Set default initial choice */
        this.peerTypeChoice.setValue(PeerChoice.SENDER);
    }


    /**
     * Get the ChoiceBox;
     */
    public ChoiceBox getPeerChoiceBox() {
        return this.peerTypeChoice;
    }
}
