package jpeersapp.frontend.options_window;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;


/**
 * Abstract class that models a sub-part (sender or receiver)
 * of the Options Window;
 */
public abstract class GuiPeerOptions {

    protected Label guiPeerOptionsName;
    protected GridPane optionsGrid;


    /**
     * Constructor
     */
    protected GuiPeerOptions() {
        optionsGrid = new GridPane();
    }


    /**
     * Get the options grid of a peer;
     */
    protected GridPane getOptionsGrid() {
        return optionsGrid;
    }


    /**
     * Set the peer options look;
     */
    protected void setPeerOptionsLook() {
        this.guiPeerOptionsName.setStyle("-fx-font-weight: bold");

        optionsGrid.setPadding(new Insets(10, 10, 10, 10));
        optionsGrid.setHgap(20);
        optionsGrid.setVgap(10);
    }


    /**
     * Add items to the peer options grid;
     */
    protected abstract void addItemsToPeerOptions();
}