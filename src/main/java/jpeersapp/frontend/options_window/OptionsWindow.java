package jpeersapp.frontend.options_window;


import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.Pos;
import java.io.InputStream;
import javafx.stage.Modality;
import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import jpeersapp.frontend.generic_peers.GuiPeer;


/**
 * This class represents the Options Window in which
 * a peer's properties can be modified;
 * The Options Window is accessible through an open
 * options button defined in this class;
 */
public class OptionsWindow {

    public Button openOptionsButton;
    public GuiSenderOptions senderOptions;
    public GuiReceiverOptions receiverOptions;
    private final BorderPane optionsPanel;
    private final Stage optionsWindowStage;
    private final HBox bottomButtonsBox;
    private final Button okButton;
    private final Button cancelButton;
    private final GuiPeer peer;


    /**
     * Constructor
     */
    public OptionsWindow(GuiPeer peer) {
        this.openOptionsButton = new Button();
        this.optionsWindowStage = new Stage();
        this.optionsPanel = new BorderPane();
        this.senderOptions = new GuiSenderOptions();
        this.receiverOptions = new GuiReceiverOptions();
        this.bottomButtonsBox = new HBox();
        this.okButton = new Button("Ok");
        this.cancelButton = new Button("Cancel");
        this.peer = peer;

        this.setOpenOptionsWindowButton();
        this.setOptionsWindowStage();
        this.populateOptionsWindow();
        this.setOptionsWindowLook();
        this.deactivateUnnecessaryOptions();
        this.setBottomButtons();
    }


    /**
     *  Set the open options button behavior and look;
     *  Each Peer has its own options button;
     */
    private void setOpenOptionsWindowButton() {
        InputStream input = OptionsWindow.class.getResourceAsStream("/icons/options.png");
        Image options = new Image(input, 16, 16, true, true);
        ImageView imageView = new ImageView(options);
        this.openOptionsButton.setGraphic(imageView);
        this.openOptionsButton.setOnAction(e -> {
            this.optionsWindowStage.show();
        });
    }


    /**
     *  Set the options' panel stage behaviour, dimensions,
     *  modality and padding;
     */
    private void setOptionsWindowStage() {
        this.optionsWindowStage.setTitle("Options");

        this.optionsWindowStage.setMinHeight(400);
        this.optionsWindowStage.setMinWidth(300);
        this.optionsWindowStage.setX(800.0);
        this.optionsWindowStage.setY(300.0);

        this.optionsWindowStage.initModality(Modality.APPLICATION_MODAL);
        this.optionsWindowStage.initOwner(null);
        this.optionsWindowStage.setScene(new Scene(this.optionsPanel));
        this.optionsPanel.setPadding(new Insets(10, 10, 10, 10));
    }


    /**
     *  Add items to the Options window:
     *  - Sender options;
     *  - Receiver options;
     *  - Bottom buttons;
     */
    private void populateOptionsWindow() {
        this.optionsPanel.setTop(this.senderOptions.getOptionsGrid());
        this.optionsPanel.setCenter(this.receiverOptions.getOptionsGrid());

        this.bottomButtonsBox.getChildren().addAll(this.okButton,
                this.cancelButton);
        this.optionsPanel.setBottom(this.bottomButtonsBox);
    }


    /**
     *  Disable access to unnecessary options in the
     *  Options Window;
     */
    private void deactivateUnnecessaryOptions() {
        switch(this.peer.choice) {
            case SENDER -> this.receiverOptions.getOptionsGrid().setDisable(true);
            case RECEIVER -> this.senderOptions.getOptionsGrid().setDisable(true);
        }
    }


    /**
     *  Set the Options window items look;
     */
    private void setOptionsWindowLook() {
        this.senderOptions.setPeerOptionsLook();
        this.receiverOptions.setPeerOptionsLook();

        this.okButton.setPadding(new Insets(5, 5, 5, 5));
        this.okButton.setMinWidth(40);
        this.cancelButton.setPadding(new Insets(5, 5, 5, 5));
        this.bottomButtonsBox.setSpacing(10);
        this.bottomButtonsBox.setAlignment(Pos.BOTTOM_RIGHT);
    }


    /**
     *  Set the bottom buttons (Ok, Cancel) behavior in
     *  the Options window;
     */
    private void setBottomButtons() {
        this.okButton.setOnAction(e -> {
            this.peer.applyNewOptions();
            this.optionsWindowStage.close();
        });

        this.cancelButton.setOnAction(e -> {
            this.optionsWindowStage.close();
        });
    }
}