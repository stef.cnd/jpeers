package jpeersapp.frontend.options_window;


import javafx.util.Duration;
import javafx.scene.control.*;


/**
 * This class models the sender part of the Options Window;
 */
public class GuiSenderOptions extends GuiPeerOptions {

    private final Label sendFrequencyLabel;
    private final RadioButton customPayloadRB;
    private final RadioButton incrementalCounterRB;
    private final RadioButton arbitrarySizeRB;
    private final TextField incrementalCounterField;
    private final TextField arbitrarySizeField;
    private final TextField sendFrequencyField;
    private final ToggleGroup togglerRB;


    /**
     * Constructor
     */
    public GuiSenderOptions() {
        super();

        this.guiPeerOptionsName = new Label("Sender Options");
        this.customPayloadRB = new RadioButton("Custom Payload");
        this.incrementalCounterRB = new RadioButton("Incremental Counter");
        this.incrementalCounterField = new TextField();
        this.arbitrarySizeRB = new RadioButton("Arbitrary Size");
        this.arbitrarySizeField = new TextField();
        this.sendFrequencyLabel = new Label("Send Frequency (ms)    ");
        this.sendFrequencyField = new TextField();
        this.togglerRB = new ToggleGroup();

        this.addItemsToPeerOptions();
        this.initialiseRadioButtons();
    }


    /**
     * Add and set all radio buttons to the related Toggle Group
     */
    public void initialiseRadioButtons() {
        this.customPayloadRB.setToggleGroup(togglerRB);
        this.incrementalCounterRB.setToggleGroup(togglerRB);
        this.arbitrarySizeRB.setToggleGroup(togglerRB);
        this.customPayloadRB.setSelected(true);
    }


    /**
     * Set the sender options look;
     */
    @Override
    public void setPeerOptionsLook() {
        super.setPeerOptionsLook();
        this.sendFrequencyField.setMaxWidth(100);
        this.incrementalCounterField.setMaxWidth(100);
        this.arbitrarySizeField.setMaxWidth(100);

        setSendingOptionsToolTips();
    }



    /**
     * Set the sending options tooltips;
     */
    private void setSendingOptionsToolTips() {
        Tooltip customPld = new Tooltip("Use the custom payload input on main window");
        Tooltip incrCnter = new Tooltip("Send an increasing counter starting from a number of choice");
        Tooltip arbtrSize  = new Tooltip("Send a payload of arbitrary size, containing 0s only");

        customPld.setShowDelay(Duration.millis(400));
        incrCnter.setShowDelay(Duration.millis(400));
        arbtrSize.setShowDelay(Duration.millis(400));

        this.customPayloadRB.setTooltip(customPld);
        this.incrementalCounterRB.setTooltip(incrCnter);
        this.arbitrarySizeRB.setTooltip(arbtrSize);
    }


    /**
     * Add items to the sender options;
     */
    @Override
    protected void addItemsToPeerOptions() {
        this.optionsGrid.add(this.guiPeerOptionsName, 0, 0);
        this.optionsGrid.add(this.customPayloadRB, 0, 1);
        this.optionsGrid.add(this.incrementalCounterRB, 0, 2);
        this.optionsGrid.add(this.incrementalCounterField, 1, 2);
        this.optionsGrid.add(this.arbitrarySizeRB, 0, 3);
        this.optionsGrid.add(this.arbitrarySizeField, 1, 3);
        this.optionsGrid.add(this.sendFrequencyLabel, 0, 4);
        this.optionsGrid.add(this.sendFrequencyField, 1, 4);
    }


    /**
     *
     */
    public Toggle gui_getSelectedRB() {
        return this.togglerRB.getSelectedToggle();
    }


    /**
     * Get the current frequency at which packets are sent;
     */
    public String gui_getSendFrequency() {
        return this.sendFrequencyField.getText();
    }


    /**
     * Get the current option related to sending an
     * incremental counter;
     */
    public String gui_getIncrementalCounter() {
        return this.incrementalCounterField.getText();
    }


    /**
     * Get the current option related to sending an
     * incremental counter;
     */
    public String gui_getArbitrarySize() {
        return this.arbitrarySizeField.getText();
    }
}