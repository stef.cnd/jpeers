package jpeersapp.frontend.tcp_peers;


import jpeersapp.backend.generic_peer.AbstractPeer;
import jpeersapp.backend.tcp_peers.TcpReceiverPeer;
import jpeersapp.frontend.generic_peers.GuiPeerState;
import jpeersapp.frontend.generic_peers.GuiReceiverPeer;
import jpeersapp.frontend.tabs.peer_choice.PeerChoice;


/**
 * This class models the GUI equivalent of a TCP sender peer;
 */
public class GuiTcpReceiverPeer extends GuiReceiverPeer {

    private final TcpReceiverPeer tcpReceiverPeer;


    /**
     * Constructor
     */
    public GuiTcpReceiverPeer(Integer peerId, PeerChoice choice) {
        super(peerId, choice);

        this.tcpReceiverPeer = new TcpReceiverPeer();

        this.setRecvButton();
        this.setStopButton();
    }


    /**
     * Collect user input on TCP receiver properties;
     * Set the TCP receiver properties (backend);
     * NOTE: this does not include peer options!
     */
    protected void collectAndSetPeerProperties() {
        this.tcpReceiverPeer.setPeerAddress(this.addressField.getText());
        this.tcpReceiverPeer.setPeerPort(Integer.parseInt(this.portField.getText()));
    }


    /**
     * Set the Recv button actions and color behavior;
     */
    private void setRecvButton() {
        this.recvButton.setOnAction(e -> {

            if(!this.state.equals(GuiPeerState.RECEIVING)) {
                this.recvButton.setStyle("-fx-text-fill: green");
                this.stopButton.setStyle("-fx-text-fill: black");

                this.collectAndSetPeerProperties();
                this.tcpReceiverPeer.startServer();

                this.state = GuiPeerState.RECEIVING;
            }
        });
    }


    /**
     * Set the Stop button actions and color behavior;
     */
    private void setStopButton() {
        this.stopButton.setOnAction(e -> {

            if (!this.state.equals(GuiPeerState.IDLE)) {
                this.stopButton.setStyle("-fx-text-fill: red");
                this.recvButton.setStyle("-fx-text-fill: black");

                this.tcpReceiverPeer.closePeer();
                this.state = GuiPeerState.IDLE;
            }
        });
    }


    /**
     * Apply new options to the TCP receiver peer (backend);
     */
    @Override
    public void applyNewOptions() {
        this.tcpReceiverPeer.options.setRecvFrequency(
                this.optionsWindow.receiverOptions.gui_getRecvFrequency());
        this.tcpReceiverPeer.options.setReceiverBufferSize(
                this.optionsWindow.receiverOptions.gui_getReceiverBufferSize());
        this.tcpReceiverPeer.options.setBacklog(
                this.optionsWindow.receiverOptions.gui_getBacklog());
    }


    /**
     * Get the actual UDP sender backend peer encapsulated in the guiPeer;
     */
    @Override
    public AbstractPeer getPeer() {
        return this.tcpReceiverPeer;
    }
}