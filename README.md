# JPeers v1.1

![Screenshot](https://gitlab.com/stef.cnd/jpeers/-/raw/main/src/main/resources/jpeers02.png)
## 1. Introduction
### 1.1 What is Jpeers?
JPeers is a GUI-based Java application that creates and manages network peers. It is written in pure Java, and relies on JavaFX for
the front-end.

### 1.2 What is it good for?
Whatever situation in which there is need to test an application or system in terms of connectivity and network performance.
Both the Udp and Tcp protocols are supported.

### 1.3 How does it work?
Two types of peer can be created:
- **Sender**: these peers cyclically send a payload towards a given address and port.
- **Receiver**: these peers are bind to a given address and port and wait for incoming data/connections. 

A number of options are available for each peer, depending on its type. For instance:
- Send/Receive frequency in milliseconds;
- Send a counter incremented at each cycle as payload;
- Send a payload of arbitrary size;
- Set the maximum payload size;
- Set the backlog;
- etc.

### 1.4 Is logging provided?
Of course. JPeers provides logging based on the following format:

`yyyy-MM-dd HH:mm:ss.SSS  |  Src-Address  |  Dst-Address  |  Src-Port --> Dst-Port  |  Payload length`

### 1.5 Is there a manual or help section?
The usage of JPeers is rather simple and intuitive. Tooltips are provided for each button where needed,
as well as for the peer options.

### 1.6 How is JPeers licensed?
BSD-3 Licence.

## 2. Installation
### 2.1 Requirements
- JDK version 17 or Higher.

#### 2.1.1 JDK setup on Linux

**Fedora**:
`sudo dnf install java-17-openjdk.x86_64`

**Ubuntu**:
`sudo apt install openjdk-17-jre-headless`

Once the jdk is installed, it is sufficient to build the artifacts with an IDE, e.g. IntelliJ Idea.

## 3. Development
### 3.1 Current state
The current version of JPeers is the 1.1. The following improvements are needed:

1. Some code polishing on the Tcp side, as well as some code improvements at logging level;
2. Implementation of unit tests;

All the functionalities are fully working (to the best of the author's knowledge).
Any bug reporting would be much appreciated, as well as contribution proposals.